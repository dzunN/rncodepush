import {StyleSheet, Text, View, Button} from 'react-native';
import React from 'react';
import codePush from 'react-native-code-push';

let codePushOptions = {checkFrequency: codePush.CheckFrequency.ON_APP_START};

const App = () => {
  const onButtonPress = () => {
    codePush.sync({
      updateDialog: true,
      installMode: codePush.InstallMode.IMMEDIATE,
    });
  };
  return (
    <View style={styles.container}>
      <Text>What</Text>
      <Button title="Aku baru berubah nihhh" onPress={onButtonPress} />
    </View>
  );
};

export default codePush(codePushOptions)(App);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
